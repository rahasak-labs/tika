name := "tika"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= {

  Seq(
    "com.typesafe.slick"              %% "slick"                          % "3.3.2",
    "com.typesafe.slick"              %% "slick-hikaricp"                 % "3.3.2",
    "org.slf4j"                       % "slf4j-api"                       % "1.7.5",
    "ch.qos.logback"                  % "logback-classic"                 % "1.0.9",
    "org.xerial"                      % "sqlite-jdbc"                     % "3.34.0",
    "org.scalatest"                   % "scalatest_2.11"                  % "2.2.1"               % "test"
  )
}

resolvers ++= Seq(
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
)

assemblyMergeStrategy in assembly := {
  case PathList(ps @ _*) if ps.last endsWith ".properties" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".RSA" => MergeStrategy.discard
  case PathList(ps @ _*) if ps.last endsWith ".keys" => MergeStrategy.discard
  case PathList(ps @ _*) if ps.last endsWith ".logs" => MergeStrategy.discard
  case PathList(ps @ _*) if ps.last endsWith ".storage" => MergeStrategy.discard
  case "module-info.class" => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}
