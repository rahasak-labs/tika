package com.librum.aplos.config

import com.typesafe.config.ConfigFactory
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import slick.jdbc.PostgresProfile.api._

import scala.util.Try

trait StorageConf {
  val config = ConfigFactory.load("storage.conf")

  // db config
  lazy val url = Try(config.getString("sqlite.db.url")).getOrElse("")
  lazy val driver = Try(config.getString("sqlite.db.driver")).getOrElse("")
  lazy val storageDir = Try(config.getString("sqlite.dir")).getOrElse("")

  // db
  val db = {
    val config = new HikariConfig
    config.setDriverClassName(driver)
    config.setJdbcUrl(url)
    config.setConnectionTestQuery("SELECT 1")
    config.setIdleTimeout(10000)
    config.setMaximumPoolSize(20)

    val ds = new HikariDataSource(config)
    Database.forDataSource(ds, Option(20))
  }
}
