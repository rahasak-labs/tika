package com.librum.aplos

import com.librum.aplos.storage.TikaRepo
import com.librum.aplos.storage.model.{Account, Balance, Token}

object Main extends App {

  // create database file and tables
  TikaRepo.init()

  // create tokens
  TikaRepo.createToken(Token(0, "rahasak", "rak"))
  TikaRepo.createToken(Token(1, "librum", "lib"))

  // get token
  println(TikaRepo.getToken("rahasak"))
  // output
  // Some(Token(0,rahasak,rak))

  // create accounts
  TikaRepo.createAccount(Account("eranga", "era", "pubkey1"))
  TikaRepo.createAccount(Account("bandara", "ban", "pubkey2"))

  // get account
  println(TikaRepo.getAccount("eranga"))
  // output
  // Some(Account(eranga,era,pubkey1))

  // create token balances for accounts
  TikaRepo.createBalance(Balance(Option(0), "eranga", "rahasak", 1000))
  TikaRepo.createBalance(Balance(Option(1), "eranga", "librum", 3000))
  TikaRepo.createBalance(Balance(Option(2), "bandara", "librum", 2000))

  // get balance
  println(TikaRepo.getTokenBalanceOfAddress("rahasak", "eranga"))

  // output
  // Some(Balance(Some(0),eranga,rahasak,1000))

  // transfer
  TikaRepo.transfer("eranga", "bandara", "librum", 99)

  // get balance to verify transfer
  println(TikaRepo.getTokenBalanceOfAddress("librum", "eranga"))
  println(TikaRepo.getTokenBalanceOfAddress("librum", "bandara"))

  // output
  // Some(Balance(Some(1),eranga,librum,2901))
  // Some(Balance(Some(2),bandara,librum,2099))

}
