package com.librum.aplos.storage

import com.librum.aplos.config.StorageConf
import com.librum.aplos.storage.model._
import slick.jdbc.PostgresProfile.api._

import java.io.File
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object TikaRepo extends App with AccountsTable with TokensTable with BalancesTable with StorageConf {

  /**
   * Create database file directory
   * Create tables with slick
   */
  def init(): List[Unit] = {
    // first create .storage directory
    val dir = new File(storageDir)
    if (!dir.exists) {
      dir.mkdir
    }

    // create table schema
    val f1 = db.run(tokens.schema.createIfNotExists)
    val f2 = db.run(balances.schema.createIfNotExists)
    val f3 = db.run(accounts.schema.createIfNotExists)
    Await.result(Future.sequence(List(f1, f2, f3)), 10.seconds)
  }

  /**
   * Create account
   */
  def createAccount(account: Account): Int = {
    val f = db.run(accounts += account)

    Await.result(f, 10.seconds)
  }

  /**
   * Get account with given address
   */
  def getAccount(address: String): Option[Account] = {
    val f = db.run(accounts.filter(_.address === address).result.headOption)

    Await.result(f, 10.seconds)
  }

  /**
   * Create token
   */
  def createToken(token: Token): Int = {
    val f = db.run(tokens += token)

    Await.result(f, 10.seconds)
  }

  /**
   * Get token with given name
   */
  def getToken(name: String): Option[Token] = {
    val f = db.run(tokens.filter(_.name === name).result.headOption)

    Await.result(f, 10.seconds)
  }

  /**
   * Create balance
   */
  def createBalance(balance: Balance): Int = {
    val f = db.run(balances += balance)

    Await.result(f, 10.seconds)
  }

  /**
   * Get all token balances of given address
   */
  def getBalancesOfAddress(address: String): Seq[(Token, Balance)] = {
    val f = db.run(
      (for ((t, b) <- tokens join balances if t.name === b.token && b.account === address)
        yield (t, b)).result.map(_.toSeq)
    )

    Await.result(f, 10.seconds)
  }

  /**
   * Get token balance of given address
   */
  def getTokenBalanceOfAddress(token: String, address: String): Option[Balance] = {
    val eb = (for {
      b <- balances if b.token === token && b.account === address
    } yield b).result.headOption

    val f = db.run(eb)
    Await.result(f, 10.seconds)
  }

  /**
   * Transfer fund from `fromAddress` to `toAddress`
   * On a transfer, balance will be credited to `fromAddress` and debited from `toAddress`
   * Two queries used to handle credit/debit functions and executed as Slick transaction
   * Currently Slick does not support to use + or - in queries. So I have used plain sql queries to facilitate
   * these functions
   */
  def transfer(fromAddress: String, toAddress: String, token: String, amount: Long): Unit = {
    val t1 =
      sqlu"""
          update balances
          set value = value + $amount
          where account = $toAddress and token = $token
          """
    val t2 =
      sqlu"""
          update balances
          set value = value - $amount
          where account = $fromAddress and token = $token
          """

    val combinedAction = DBIO.seq(t1, t2)
    val f = db.run(combinedAction.transactionally)
    Await.result(f, 10.seconds)
  }


}