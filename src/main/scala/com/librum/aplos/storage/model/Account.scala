package com.librum.aplos.storage.model

import com.librum.aplos.config.StorageConf
import slick.jdbc.PostgresProfile.api._
import slick.lifted.Tag

case class Account(address: String, name: String, pubkey: String)

trait AccountsTable {
  this: StorageConf =>

  class Accounts(tag: Tag) extends Table[Account](tag, "accounts") {
    def address = column[String]("address", O.PrimaryKey, O.Unique)

    def name = column[String]("name")

    def pubkey = column[String]("pubkey")

    // select
    def * = (address, name, pubkey) <> (Account.tupled, Account.unapply)
  }

  val accounts = TableQuery[Accounts]
}

