package com.librum.aplos.storage.model

import com.librum.aplos.config.StorageConf
import slick.jdbc.PostgresProfile.api._
import slick.lifted.{ProvenShape, Tag}

case class Balance(id: Option[Int], account: String, token: String, value: Long)

trait BalancesTable {
  this: StorageConf =>

  class Balances(tag: Tag) extends Table[Balance](tag, "balances") {
    def id = column[Int]("id", O.PrimaryKey)

    def account = column[String]("account")

    def token = column[String]("token")

    def value = column[Long]("value")

    // select
    def * : ProvenShape[Balance] = (id.?, account, token, value) <> (Balance.tupled, Balance.unapply)
  }

  val balances = TableQuery[Balances]
}
